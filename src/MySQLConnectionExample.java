import java.sql.*;

public class MySQLConnectionExample {

    private static final String DRIVER = System.getProperty("driver", "com.mysql.jdbc.Driver");
    private static final String URL = System.getProperty("url", "jdbc:mysql://hostname:port/database_name");
    private static final String USERNAME = System.getProperty("username", "root");
    private static final String PASSWORD = System.getProperty("password", "Qwerty1");

    public static void main(String[] args) {
        try {
            Class.forName(DRIVER);
        } catch (ClassNotFoundException ex) {
            System.out.println("Driver not found");
            ex.printStackTrace();
        }

        Connection connection = null;
        try {
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
        } catch (SQLException ex) {
            System.out.println("Connection failed");
            ex.printStackTrace();
        }

        if (connection != null) {
            String query = "select * from some_table";

            Statement statement = null;
            try {
                statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(query);
                while (rs.next()) {
                    System.out.println(rs.getInt("integer_column_name"));
                    System.out.println(rs.getString("varchar_column_name"));
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            } finally {
                try {
                    if (statement != null)
                        statement.close();
                    connection.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }
}
